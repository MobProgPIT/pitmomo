import React, { useState } from 'react';
import { View, StyleSheet, Image, ImageBackground, Text, TouchableOpacity, TextInput, Modal, ScrollView, KeyboardAvoidingView, Keyboard } from 'react-native';
import { Icon } from 'react-native-elements';
import EditingIcon from '../../components/Images/editing.png';
import PlaceIcon from '../../components/Images/home.png';
import CalendarIcon from '../../components/Images/calendar.png';
import PhoneIcon from '../../components/Images/phone.png';
import WorkIcon from '../../components/Images/work.png';
import SchoolIcon from '../../components/Images/school.png';
import BloodIcon from '../../components/Images/blood.png';
import GenderIcon from '../../components/Images/gender.png';

const Profile = ({ navigation }) => {
  const [profileImage, setProfileImage] = useState(
    require('../../components/Images/doe.jpg')
    
  );
  
  const [backgroundImage, setBackgroundImage] = useState(
    require('../../components/Images/bgw.jpg')
  );

  const [isEditing, setIsEditing] = useState(false);
  const [showAdditionalInfo, setShowAdditionalInfo] = useState(false);
  const [userDetails, setUserDetails] = useState({
    name: 'Daren Rebote',
    
    placeLived: 'New York',
    birthday: 'January 1, 1990',
    phoneNumber: '123-456-7890',
    work: 'Software Developer',
    school: 'Example University',
    bloodType: 'A+',
    gender: 'Male',
  });
  

  const [editingName, setEditingName] = useState(userDetails.name);
  const [isNameEditing, setIsNameEditing] = useState(false);
  const [isNameModalVisible, setIsNameModalVisible] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [isKeyboardActive, setIsKeyboardActive] = useState(false);
  const [isAuthenticated, setIsAuthenticated] = useState(true);
  const [isEditingProfile, setIsEditingProfile] = useState(false); // For the "Edit Profile" popup
  const [isEditingDetails, setIsEditingDetails] = useState(false); // Assuming the user is initially authenticated




  const handleEditProfile = () => {
    if (isAuthenticated) {
      setIsNameEditing(true);
      setIsNameModalVisible(true);
      // setIsEditing(true);
      // setIsNameEditing(true);
      // setIsNameModalVisible(true);
      // setIsEditingProfile(true);
      // setIsEditingDetails(false);

    } else {
    }
  };
  
  const handleEditName = () => {
    setIsEditingName(true);
    setIsNameModalVisible(true);
  };

  const handleLogout = () => {
    setIsAuthenticated(false); // Clear authentication status
    setIsEditing(false); // Clear editing status
    navigation.navigate('Login', { isAuthenticated: false }); // Pass isAuthenticated to Login
  };

  const handleEditDetails = () => {
    setIsEditing(!isEditing);
    setIsEditingDetails(true);
    setIsEditingProfile(false);

  };

  const handleSaveName = () => {
    setUserDetails({ ...userDetails, name: editingName });
    setIsNameEditing(false);
    setIsNameModalVisible(false);
  };

  const handleCloseNameModal = () => {
    setIsNameEditing(false);
    setIsNameModalVisible(false);
  };

  const handleBurgerIconPress = () => {
    setIsDropdownVisible(!isDropdownVisible); // Toggle the dropdown visibility
  };


  const handleSetting = () => {
    // Implement your logout logic here
  };

  const handleAbout = () => {
    // Implement your logout logic here
  };


  
  return (
    <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'android' ? 'padding' : null}>
    
      <View style={styles.burgerIconContainer}>
        <TouchableOpacity onPress={handleBurgerIconPress}>
          <Icon name="menu" size={30} color="#fff" />
        </TouchableOpacity>
      </View>
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <View style={styles.profileContainer}>
          <Image source={profileImage} style={styles.profileImage} />
        </View>
        <View style={styles.cameraIconContainer1}>
          <Image source={require('../../components/Images/photo-camera.png')} style={styles.cameraIcon1} />
        </View>
        <View style={styles.cameraIconContainer2}>
          <Image source={require('../../components/Images/photo-camera.png')} style={styles.cameraIcon2} />
        </View>
      </ImageBackground>

      <View style={styles.containerBelowBackground}>
        <TouchableOpacity style={styles.editProfileButton} onPress={handleEditProfile}>
          <Image source={require('../../components/Images/pen.png')} style={styles.pencilIcon} />
          <Text style={styles.editProfileText}>Edit Profile</Text>
        </TouchableOpacity>
    </View>

      <View style={styles.nameContainer}>
        {isNameEditing ? (
          <TextInput
            style={styles.userNameText}
            value={editingName}
            onChangeText={(text) => setEditingName(text)}
          />
        ) : (
          <Text style={styles.userNameText}>{userDetails.name}</Text>
        )}
      </View>

      <View style={styles.detailsCon}>
        <Text style={styles.detailsText}>Details</Text>
        <TouchableOpacity style={styles.editingButton} onPress={handleEditDetails}>
          <Image source={EditingIcon} style={styles.editingIcon} />
        </TouchableOpacity>
        <ScrollView keyboardShouldPersistTaps="handled">
          {isEditing ? (
            <View>
              <Text style={styles.label}>Place Lived</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.placeLived}
                onChangeText={(text) => setUserDetails({ ...userDetails, placeLived: text })}
                placeholder="Place Lived"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={PlaceIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Place Lived: {userDetails.placeLived}</Text>
            </View>          
            )}

          {isEditing ? (
            <View>
              <Text style={styles.label}>Birthday</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.birthday}
                onChangeText={(text) => setUserDetails({ ...userDetails, birthday: text })}
                placeholder="Birthday"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={CalendarIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Birthday: {userDetails.birthday}</Text>
            </View>            
            )}

          {isEditing ? (
            <View>
              <Text style={styles.label}>Phone Number</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.phoneNumber}
                onChangeText={(text) => setUserDetails({ ...userDetails, phoneNumber: text })}
                placeholder="Phone Number"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={PhoneIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Phone Number: {userDetails.phoneNumber}</Text>
            </View>            
            )}

          {isEditing ? (
            <View>
              <Text style={styles.label}>Work</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.work}
                onChangeText={(text) => setUserDetails({ ...userDetails, work: text })}
                placeholder="Work"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={WorkIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Work: {userDetails.work}</Text>
            </View>            
            )}

          {isEditing ? (
            <View>
              <Text style={styles.label}>School</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.school}
                onChangeText={(text) => setUserDetails({ ...userDetails, school: text })}
                placeholder="School"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={SchoolIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>School: {userDetails.school}</Text>
            </View>            
            )}

          {isEditing ? (
            <View>
              <Text style={styles.label}>Blood Type</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.bloodType}
                onChangeText={(text) => setUserDetails({ ...userDetails, bloodType: text })}
                placeholder="Blood Type"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={BloodIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Blood Type: {userDetails.bloodType}</Text>
            </View>            
            )}

            {isEditing ? (
            <View>
              <Text style={styles.label}>Blood Type</Text>
              <TextInput
                style={styles.userDetailsInput}
                value={userDetails.bloodType}
                onChangeText={(text) => setUserDetails({ ...userDetails, bloodType: text })}
                placeholder="Gender"
              />
            </View>
          ) : (
            <View style={styles.detailsInfoContainer}>
              <Image source={GenderIcon} style={styles.icon} />
              <Text style={styles.detailsInfo}>Gender: {userDetails.gender}</Text>
            </View>            
            )}
        </ScrollView>
      </View>

      {isDropdownVisible && (
        <View style={styles.dropdownMenu}>
          <View style={styles.btn}>
            <TouchableOpacity onPress={handleLogout}>
              <Text style={styles.dropdownMenuItem}>Logout</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.btn}>
            <TouchableOpacity onPress={handleSetting}>
              <Text style={styles.dropdownMenuItem}>Settings</Text>
            </TouchableOpacity>
          </View>
          
          <View style={styles.btn}>
            <TouchableOpacity onPress={handleAbout}>
              <Text style={styles.dropdownMenuItem}>About</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}

      
      <Modal animationType="slide" transparent={true} visible={isEditing}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <ScrollView keyboardShouldPersistTaps="handled">
              {isEditing ? (
                <View>
                  <Text style={styles.label}>Place Lived</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.placeLived}
                    onChangeText={(text) => setUserDetails({ ...userDetails, placeLived: text })}
                    placeholder="Place Lived"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Place Lived: {userDetails.placeLived}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>Birthday</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.birthday}
                    onChangeText={(text) => setUserDetails({ ...userDetails, birthday: text })}
                    placeholder="Birthday"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Birthday: {userDetails.birthday}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>Phone Number</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.phoneNumber}
                    onChangeText={(text) => setUserDetails({ ...userDetails, phoneNumber: text })}
                    placeholder="Phone Number"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Phone Number: {userDetails.phoneNumber}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>Work</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.work}
                    onChangeText={(text) => setUserDetails({ ...userDetails, work: text })}
                    placeholder="Work"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Work: {userDetails.work}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>School</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.school}
                    onChangeText={(text) => setUserDetails({ ...userDetails, school: text })}
                    placeholder="School"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>School: {userDetails.school}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>Blood Type</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.bloodType}
                    onChangeText={(text) => setUserDetails({ ...userDetails, bloodType: text })}
                    placeholder="Blood Type"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Blood Type: {userDetails.bloodType}</Text>
              )}

              {isEditing ? (
                <View>
                  <Text style={styles.label}>Gender</Text>
                  <TextInput
                    style={styles.userDetailsInput}
                    value={userDetails.gender}
                    onChangeText={(text) => setUserDetails({ ...userDetails, gender: text })}
                    placeholder="Gender"
                  />
                </View>
              ) : (
                <Text style={styles.detailsInfo}>Gender: {userDetails.gender}</Text>
              )}

              <TouchableOpacity style={styles.saveButton} onPress={() => setIsEditing(false)}>
                <Text style={styles.buttonText}>Save</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.cancelButton} onPress={() => setIsEditing(false)}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </Modal>

      <Modal animationType="slide" transparent={true} visible={isNameModalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.profText}>Profile Name</Text>
            <TextInput
              style={styles.userDetailsInput}
              value={editingName}
              onChangeText={(text) => setEditingName(text)}
              placeholder="Edit Name"
            />
            <TouchableOpacity style={styles.saveButton} onPress={handleSaveName}>
              <Text style={styles.buttonText}>Save</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cancelButton} onPress={handleCloseNameModal}>
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    width: 430,
    height: 260,
    justifyContent: 'center',
  },
  profileContainer: {
    position: 'absolute',
    bottom: 5,
    backgroundColor: '#000',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 30,
    position: 'absolute',
    bottom: -34,
    zIndex: 1,
    borderRadius: 130 / 2,
  },
  profileImage: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    
  },
  cameraIconContainer1: {
    width: 30,
    height: 30,
    backgroundColor: '#000',
    borderRadius: 30 / 2,
    marginLeft: 10,
    position: 'absolute',
    bottom: -10,
    left: 133,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon1: {
    width: 15,
    height: 15,
  },
  cameraIconContainer2: {
    width: 30,
    height: 30,
    backgroundColor: '#000',
    borderRadius: 30 / 2,
    marginLeft: 10,
    position: 'absolute',
    bottom: 30,
    right: 40,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cameraIcon2: {
    width: 15,
    height: 15,
  },
  containerBelowBackground: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    marginTop: -15,
  },
  editProfileButton: {
    flexDirection: 'row',
    width: 144,
    height: 36,
    backgroundColor: '#212326',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 15,
    right: 23,
  },
  editProfileText: {
    fontSize: 14,
    fontWeight: '400',
    color: '#fff',
    paddingLeft: 5,
    paddingRight: 5,
  },
  pencilIcon: {
    width: 20,
    height: 20,
    marginLeft: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
  burgerIconContainer: {
    position: 'absolute',
    top: 50,
    right: 20,
    zIndex: 2,
  },
  nameContainer: {
    width: 155,
    height: 40,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [{ translateX: -186 }, { translateY: -140 }],
  },
  userNameText: {
    fontSize: 20,
    fontWeight: '900',
    color: '#000',
    textAlign: 'center',
    paddingTop: 5,
  },
  detailsCon: {
    width: 372,
    height: 195,
    backgroundColor: '#F8F8F8',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [{ translateX: -186 }, { translateY: -90 }],
    borderRadius: 10,
    elevation: 3,
    shadowColor: 'rgba(0, 0, 0, 0.8)',
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  detailsText: {
    backgroundColor: '#212326',
    fontSize: 15,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'left',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  detailsInfo: {
    fontSize: 15,
    fontWeight: '500',
    color: '#000',
    textAlign: 'left',
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderWidth: 0,

  },
  editingButton: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 3,
  },
  editingIcon: {
    width: 20,
    height: 20,
  },
  userDetailsInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .8)',
  },
  profText: {
    fontSize: 15,
    fontWeight: '900',
    paddingBottom: 10,
  },
  modalContent: {
    width: 350,
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 10,
  },
  saveButton: {
    backgroundColor: 'orange',
    padding: 10,
    borderRadius: 5,
    margin: 5,
    alignItems: 'center',
  },
  cancelButton: {
    backgroundColor: 'orange',
    padding: 10,
    borderRadius: 5,
    margin: 5,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  label: {
    fontSize: 12, 
    fontWeight: 'bold',
    color: '#000',
    marginBottom: 5,
  },
  detailsInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 20, 
    height: 20, 
    marginLeft: 20, 
  },

  dropdownMenu: {
    width: 200,
    height: 200,
    position: 'absolute',
    top: 78,
    right: 1,
    backgroundColor: 'rgba(0, 0, 0, .5)',
    borderRadius: 10,
    elevation: 5,
    shadowColor: 'rgba(0, 0, 0, 0.8)',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: .5,
    shadowRadius: 5,
    zIndex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dropdownMenuItem: {
    fontSize: 15,
    fontWeight: '900',
    color: '#fff',
  },

  btn:{
    justifyContent: 'center',
    alignItems: 'center',
    width: 155,
    backgroundColor: '#FFAB49',
    padding: 10,
    marginBottom: 8,
    borderRadius: 5,
    

  },
});

export default Profile;