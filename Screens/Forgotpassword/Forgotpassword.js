import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Input } from 'react-native-elements';
import zxcvbn from 'zxcvbn';
import Icon from 'react-native-vector-icons/FontAwesome';


const Forgotpassword = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  const passwordStrength = (pwd) => {
    if (pwd.trim().length === 0) {
      return null; // Don't display strength if the password is empty
    }

    const result = zxcvbn(pwd);
    const score = result.score; // Password strength score (0 to 4)
    const feedback = result.feedback.suggestions.join(' ');

    if (score === 0) {
      return { text: 'Weak', color: 'red', feedback };
    } else if (score === 1) {
      return { text: 'Medium', color: 'orange', feedback };
    } else if (score === 2) {
      return { text: 'Moderate', color: 'yellow', feedback };
    } else if (score === 3) {
      return { text: 'Strong', color: 'green', feedback };
    } else if (score === 4) {
      return { text: 'Very Strong', color: 'green', feedback };
    }
  };

  const handleConfirmPasswordChange = (text) => {
    setConfirmPassword(text);
    if (newPassword === text) {
      setIsPasswordMatch(true);
    } else {
      setIsPasswordMatch(false);
    }
  };

  const handleResetPassword = () => {
    // Implement your password reset logic here
    console.log('Email:', email);
    console.log('New Password:', newPassword);
    console.log('Confirm Password:', confirmPassword);
  };

  return (
    <View style={styles.container}>
      <View style={styles.honeycombTopMid} />

      
      <View style={styles.header}>
        <Text style={styles.logoname}>
          <Text style={styles.proText}>MO</Text>
          <Text style={styles.goText}>MO</Text>
        </Text>
        <TouchableOpacity style={styles.backButton} onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={20} color="#FFF" />
        </TouchableOpacity>
      </View>

      

      <Text style={styles.textpass}>Change your password</Text>

      <Input
        placeholder="Email"
        leftIcon={<Image source={require('../../components/Images/email.png')} style={styles.inputIcon} />}
        onChangeText={(text) => {
          setEmail(text);
          setIsPasswordMatch(true);
        }}
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      <Input
        placeholder="New Password"
        leftIcon={<Image source={require('../../components/Images/lock.png')} style={styles.inputIcon} />}
        onChangeText={(text) => setNewPassword(text)}
        secureTextEntry
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      <View style={styles.passwordStrength}>
        {passwordStrength(newPassword) && (
          <Text style={{ color: passwordStrength(newPassword).color, fontWeight: 'bold', paddingBottom: 8 }}>
            {passwordStrength(newPassword).text}
          </Text>
        )}
      </View>

      <Input
        placeholder="Confirm Password"
        leftIcon={<Image source={require('../../components/Images/lock.png')} style={styles.inputIcon} />}
        onChangeText={handleConfirmPasswordChange}
        secureTextEntry
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      {!isPasswordMatch && (
        <Text style={styles.errorText}>Password does not match</Text>
      )}

      <TouchableOpacity style={styles.resetButton} onPress={handleResetPassword}>
        <Text style={styles.buttonText}>Reset Password</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#fff',
  },
  honeycombTopMid: {
    position: 'absolute',
    top: -220,
    left: -100,
    borderRadius: 300,
    width: 750,
    height: 950,
    backgroundColor: '#212326',
    transform: [{ rotate: '45deg' }],
  },
  logoname: {
    marginTop: 50,
    marginBottom: 25,
    fontSize: 80,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backButton: {
    position: 'absolute',
    top:-70,
    right: 295,
  },

  proText: {
    fontSize: 80,
    color: '#FFF',
    fontWeight: '900',
  },
  goText: {
    fontSize: 80,
    color: '#FFAB49',
    fontWeight: '900',
  },
  textpass: {
    color: '#fff',
    fontSize: 18,
    paddingBottom: 25,
  },
  passwordStrength: {
    alignSelf: 'flex-end',
    paddingRight: 5,
  },
  inputIcon: {
    width: 20,
    height: 20,
  },
  input: {
    display: 'flex',
    width: '100%',
    height: 55,
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  resetButton: {
    marginTop: 20,
    marginBottom: 90,
    backgroundColor: '#FFAB49',
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 17,
    color: 'white',
    fontWeight: '900',
  },
  errorText: {
    color: 'red',
    paddingBottom: 10,
  },
});

export default Forgotpassword;
