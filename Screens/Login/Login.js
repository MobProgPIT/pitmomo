import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useRef, useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Input } from 'react-native-elements';


const Login = ({ navigation, route }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isAuthenticated, setIsAuthenticated] = useState(false); // Add the isAuthenticated state

    const usernameInputRef = useRef(null);
    const passwordInputRef = useRef(null);

    const handleLogin = async () => {
        // Retrieve the registered users from AsyncStorage
        const storedUsersJSON = await AsyncStorage.getItem('registeredUsers');
        const storedUsers = storedUsersJSON ? JSON.parse(storedUsersJSON) : [];

        if (storedUsers) {
            const user = storedUsers.find((u) => u.email === username && u.password === password);

            if (user) {
                console.log('Login successful');
                setIsAuthenticated(true); // Set authentication status
                navigation.navigate('Profile', { isAuthenticated: true });
            } else {
                console.log('Login failed');
            }
        } else {
            console.log('No registered users found');
        }

        // Clear the input fields using refs
        if (usernameInputRef.current) {
            usernameInputRef.current.clear();
        }
        if (passwordInputRef.current) {
            passwordInputRef.current.clear();
        }
    };


    const handleForgotPassword = () => {
        console.log('Navigating to ForgotPassword screen');
        navigation.navigate('Forgotpassword');
    };


    const navigateToRegister = () => {
        navigation.navigate('Register');
    };

    const handleLogout = () => {
        setIsAuthenticated(false); // Clear authentication status
        navigation.navigate('Login'); // Navigate back to the login screen
    };

    return (
        <View style={styles.container}>
            <View style={styles.honeycombTopMid} />

            <View style={styles.logo}>
                <Text style={styles.logoname}>
                    <Text style={styles.proText}>MO</Text>
                    <Text style={styles.goText}>MO</Text>
                </Text>
            </View>

            <Input
                placeholder="Email"
                leftIcon={<Image source={require('../../components/Images/user.png')} style={styles.inputIcon} />}
                onChangeText={(text) => setUsername(text)}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={styles.input}
                ref={usernameInputRef}
            />


            <Input
                placeholder="Password"
                leftIcon={<Image source={require('../../components/Images/lock.png')} style={styles.inputIcon} />}
                onChangeText={(text) => setPassword(text)}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={[styles.input, styles.passwordInput]}
                secureTextEntry
                ref={passwordInputRef}
            />


            <TouchableOpacity style={styles.forgotPasswordLink} onPress={handleForgotPassword}>
                <Text style={styles.forgotPasswordText}>Forgot your password?</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.loginButton} onPress={handleLogin}>
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>

            <View style={styles.orTextContainer}>
                <View style={styles.orVerticalLine} />
                <Text style={styles.orText}>Or sign in with</Text>
                <View style={styles.orVerticalLine} />
            </View>

            <View style={styles.iconButtonsContainer}>
                <TouchableOpacity style={styles.iconButton}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../../components/Images/facebook.png')} style={styles.iconImage} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.iconButton}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../../components/Images/google.png')} style={styles.iconImage} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.iconButton}>
                    <View style={styles.iconContainer}>
                        <Image source={require('../../components/Images/instagram.png')} style={styles.iconImage} />
                    </View>
                </TouchableOpacity>
            </View>

            <View style={styles.signupTextContainer}>
                <Text style={styles.signupText1}>Don't have an account? </Text>
                <TouchableOpacity onPress={navigateToRegister}>
                    <Text style={styles.signupText2}>Sign up here.</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
    },
    honeycombTopMid: {
        position: 'absolute',
        top: -220,
        left: -100,
        borderRadius: 300,
        width: 750,
        height: 950,
        backgroundColor: '#212326',
        transform: [{ rotate: '45deg' }],
    },
    logoname: {
        marginTop: 50,
        marginBottom: 60,
        fontSize: 80,
        color: '#FFF',
    },
    proText: {
        fontSize: 80,
        color: '#FFF',
        fontWeight: '900',
    },
    goText: {
        fontSize: 80,
        color: '#FFAB49',
        fontWeight: '900',
    },

    inputIcon: {
        width: 20,
        height: 20,
    },
    input: {
        height: 55,
        backgroundColor: '#fff',
        borderRadius: 10,
        paddingHorizontal: 10,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        marginBottom: 10,
    },
    passwordInput: {
        marginTop: 5,
    },
    forgotPasswordLink: {
        alignSelf: 'flex-end',
        marginBottom: 10,
    },
    forgotPasswordText: {
        color: '#fff',
    },
    loginButton: {
        marginTop: 20,
        marginBottom: 35,
        backgroundColor: '#FFAB49',
        width: '100%',
        height: 55,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 17,
        color: 'white',
        fontWeight: '900',
    },
    orText: {
        marginTop: 5,
        marginBottom: 10,
        fontSize: 16,
        color: '#888',
    },
    orTextContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
        marginHorizontal: 10,
    },
    orVerticalLine: {
        flex: 1,
        height: 1,
        backgroundColor: '#888',
        marginLeft: 10,
        marginRight: 10,
    },
    iconContainer: {
        width: 63,
        height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        borderColor: '#888',
        backgroundColor: '#fff',
        shadowColor: 'rgba(0, 0, 0, 0.24)',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    iconButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50,
    },
    iconButton: {
        margin: 10,
    },
    iconImage: {
        width: 25,
        height: 25,
    },
    signupTextContainer: {
        flexDirection: 'row',
        marginTop: 20,
        alignItems: 'center',
    },
    signupText1: {
        fontSize: 13,
        color: '#000000',
        fontWeight: '400',
    },
    signupText2: {
        fontSize: 13,
        color: '#FFAB49',
        fontWeight: '800',
    },
});

export default Login;