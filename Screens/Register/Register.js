import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, StyleSheet } from 'react-native';
import zxcvbn from 'zxcvbn';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Register = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);
  const [registeredUsers, setRegisteredUsers] = useState([]);
  const [emailValid, setEmailValid] = useState(true);

  const passwordStrength = (pwd) => {
    if (pwd.trim().length === 0) {
      return null; // Don't display strength if the password is empty
    }

    const result = zxcvbn(pwd);
    const score = result.score; // Password strength score (0 to 4)
    const feedback = result.feedback.suggestions.join(' ');

    if (score === 0) {
      return { text: 'Weak', color: 'red', feedback };
    } else if (score === 1) {
      return { text: 'Medium', color: 'orange', feedback };
    } else if (score === 2) {
      return { text: 'Moderate', color: 'yellow', feedback };
    } else if (score === 3) {
      return { text: 'Strong', color: 'green', feedback };
    } else if (score === 4) {
      return { text: 'Very Strong', color: 'green', feedback };
    }
  };

  const handlePasswordFocus = () => {
    setIsPasswordFocused(true);
  };

  const handlePasswordBlur = () => {
    setIsPasswordFocused(false);
  };

  const passwordStrengthIndicator = (pwd) => {
    const strength = passwordStrength(pwd);
    if (!strength) {
      return null; // Don't display strength if the password is empty
    }

    return (
      <View style={styles.passwordStrength}>
        <Text style={{ color: strength.color, fontWeight: 'bold' }}>
          {strength.text}
        </Text>
        <Text style={{ color: 'white', paddingBottom: 10 }}>{strength.feedback}</Text>
      </View>
    );
  };
  useEffect(() => {
    navigation.setParams({ registeredUsers });
  }, [registeredUsers]);

  const handleConfirmPasswordChange = (text) => {
    setConfirmPassword(text);
    if (password !== text) {
      setIsPasswordMatch(false);
    } else {
      setIsPasswordMatch(true);
    }
  };

  const handleSignUp = async () => {
    if (email.includes('@') && email.trim().length > 0) {
      const newUser = { email, username, password };
      setRegisteredUsers([...registeredUsers, newUser]);
      
      // Save the registered users to AsyncStorage
      await AsyncStorage.setItem('registeredUsers', JSON.stringify([...registeredUsers, newUser]));
      
      console.log('Sign Up successful');
      
      // Pass the registeredUsers data to the Login screen and navigate
      navigation.navigate('Login', { registeredUsers: [...registeredUsers, newUser] });
    } else {
      setEmailValid(false);
    }
  };
  

  return (
    <View style={styles.container}>
      <View style={styles.honeycombTopMid} />

      <View style={styles.header}>
        <Text style={styles.logoname}>
          <Text style={styles.proText}>MO</Text>
          <Text style={styles.goText}>MO</Text>
        </Text>
        <TouchableOpacity style={styles.backButton} onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={20} color="#FFF" />
        </TouchableOpacity>
      </View>

      <Input
        placeholder="Username"
        leftIcon={<Image source={require('../../components/Images/user.png')} style={styles.inputIcon} />}
        onChangeText={(text) => setUsername(text)}
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      <Input
        placeholder="Email" // New email input
        leftIcon={<Image source={require('../../components/Images/email.png')} style={styles.inputIcon} />}
        onChangeText={(text) => {
          setEmail(text);
          setEmailValid(true); // Reset email validity when user modifies the email
        }}
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      {!emailValid && (
        <Text style={styles.errorText}>Please enter a valid email (with @)</Text>
      )}

      <Input
        placeholder="Password"
        leftIcon={<Image source={require('../../components/Images/lock.png')} style={styles.inputIcon} />}
        onChangeText={(text) => setPassword(text)}
        secureTextEntry
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      <View style={styles.passwordStrength}>
        {passwordStrength(password) && (
          <Text style={{ color: passwordStrength(password).color, fontWeight: 'bold', paddingBottom: 8 }}>
            {passwordStrength(password).text}
          </Text>
        )}
      </View>

      <Input
        placeholder="Confirm Password"
        leftIcon={<Image source={require('../../components/Images/lock.png')} style={styles.inputIcon} />}
        onChangeText={handleConfirmPasswordChange}
        secureTextEntry
        containerStyle={styles.input}
        inputContainerStyle={{ borderBottomWidth: 0 }}
      />

      {!isPasswordMatch && (
        <Text style={styles.errorText}>Password does not match</Text>
      )}

      <TouchableOpacity style={styles.signupButton} onPress={handleSignUp}>
        <Text style={styles.buttonText}>Create Account</Text>
      </TouchableOpacity>

      <View style={styles.orTextContainer}>
        <View style={styles.orVerticalLine} />
        <Text style={styles.orText}>Or sign in with</Text>
        <View style={styles.orVerticalLine} />
      </View>

      <View style={styles.iconButtonsContainer}>
        <TouchableOpacity style={styles.iconButton}>
          <View style={styles.iconContainer}>
            <Image source={require('../../components/Images/facebook.png')} style={styles.iconImage} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.iconButton}>
          <View style={styles.iconContainer}>
            <Image source={require('../../components/Images/google.png')} style={styles.iconImage} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.iconButton}>
          <View style={styles.iconContainer}>
            <Image source={require('../../components/Images/instagram.png')} style={styles.iconImage} />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  honeycombTopMid: {
    position: 'absolute',
    top: -220,
    left: -100,
    borderRadius: 300,
    width: 750,
    height: 950,
    backgroundColor: '#212326',
    transform: [{ rotate: '45deg' }],
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backButton: {
    position: 'absolute',
    top: 90,
    right: 295,
  },
  logoname: {
    marginTop: 165,
    marginBottom: 60,
    fontSize: 80,
    color: '#FFF',
  },
  proText: {
    fontSize: 80,
    color: '#FFF',
    fontWeight: '900',
  },
  goText: {
    fontSize: 80,
    color: '#FFAB49',
    fontWeight: '900',
  },
  inputIcon: {
    width: 20,
    height: 20,
  },
  input: {
    display: 'flex',
    width: '100%',
    height: 55,
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  passwordStrength: {
    alignSelf: 'flex-end',
    paddingRight: 5,
  },
  errorText: {
    color: 'red',
    paddingBottom: 10,
  },
  signupButton: {
    marginTop: 20,
    marginBottom: 90,
    backgroundColor: '#FFAB49',
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 17,
    color: 'white',
    fontWeight: '900',
  },
  orText: {
    marginTop: 0,
    marginBottom: 10,
    fontSize: 16,
    color: '#000',
    alignItems: 'center',
  },
  orTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 10,
  },
  orVerticalLine: {
    flex: 1,
    height: 1,
    backgroundColor: '#000',
    marginLeft: 10,
    marginRight: 10,
  },
  iconContainer: {
    width: 63,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    borderColor: '#888',
    backgroundColor: '#fff',
    ...Platform.select({
      android: {
        elevation: 2,
        shadowColor: 'rgba(0, 0, 0, 0.24)',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.8, // Updated shadowOpacity
        shadowRadius: 5,
      },
    }),
  },
  iconButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  iconButton: {
    margin: 10,
  },
  iconImage: {
    width: 25,
    height: 25,
  },
});

export default Register;