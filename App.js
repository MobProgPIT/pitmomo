import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import GetStarted from './Screens/GetStarted/getstarted';
import Login from './Screens/Login/Login';
import Register from './Screens/Register/Register';
import Profile from './Screens/Profile/Profile';
import Forgotpassword from './Screens/Forgotpassword/Forgotpassword';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="GetStarted" component={GetStarted} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="Forgotpassword" component={Forgotpassword} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
